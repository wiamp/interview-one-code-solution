import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import {
  Container,
  Table,
  Button,
} from 'reactstrap';

import {
  getLocalStorage
} from '../components/LocalStorage';

const Detail = () => {
  if (!getLocalStorage("userData")) {
    window.location.href = "/login";
  }

  const [detailPost, setDetailPost] = useState({});

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    // fetch data
    const dataFetch = async () => {
      const resultPost = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/posts"
        )
      ).json();

      const resultUser = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/users"
        )
      ).json();

      // console.log(params);

      for (let i in resultPost) {
        if (resultPost[i].id == params.postId) {
          const resultComment = await (
            await fetch(
              `https://jsonplaceholder.typicode.com/posts/${resultPost[i].id}/comments`
            )
          ).json();

          resultPost[i].totalComment = resultComment.length;

          for (let j in resultUser) {
            if (resultPost[i].userId === resultUser[j].id) {
              resultPost[i].userName = resultUser[j].name;
              break;
            }
          }

          // set state when the data received
          setDetailPost(resultPost[i]);

          break;
        }
      }
    };

    dataFetch();
  }, []);

  return (
    <Container>
      <div className="mb-3">
        <Button color="secondary" size="sm" onClick={() => navigate(-1)}>Back</Button>
      </div>
      <Table hover bordered striped responsive size="sm">
        <tbody>
          <tr>
            <td>{detailPost.userName}</td>
            <td>
              <div>{detailPost.title}</div>
              <div>{detailPost.body}</div>
              <div>
                <a href={`/post/${detailPost.id}/detail-comment`}>{detailPost.totalComment}</a>&nbsp;
              </div>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

export default Detail;