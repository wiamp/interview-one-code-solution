import { useNavigate } from 'react-router-dom';

import {
  Container,
  Table,
  Button,
  Row,
  Col
} from 'reactstrap';

import {
  getLocalStorage
} from '../components/LocalStorage';

const Profile = () => {
  if (!getLocalStorage("userData")) {
    window.location.href = "/login";
  }

  const userData = getLocalStorage("userData");

  const navigate = useNavigate();

  return (
    <Container>
      <div className="mb-3">
        <Button color="secondary" size="sm" onClick={() => navigate(-1)}>Back</Button>
      </div>
      <Row>
        <Col sm={{ size: 4, offset: 4 }}>
          <Table hover bordered striped responsive size="sm">
            <tbody>
              <tr>
                <td>Username</td>
                <td>{userData.username}</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>{userData.email}</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>{userData.address?.street}</td>
              </tr>
              <tr>
                <td>Phone</td>
                <td>{userData.phone}</td>
              </tr>
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};

export default Profile;