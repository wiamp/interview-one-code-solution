import { useState } from 'react';

import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  Alert,
} from 'reactstrap';

import {
  setLocalStorage
} from '../components/LocalStorage';

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    setErrorMessage("")

    const result = await fetch('https://jsonplaceholder.typicode.com/users', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(data => data.json());

    let flag = 0;

    for (let i in result) {
      if (result[i].username === username && result[i].username === password) {
        setLocalStorage("userData", result[i], (24 * 60 * 60 * 1000)); // 1 day
        window.location.href = "/dashboard";
        flag = 1;
        break;
      }
    }

    if (flag === 0) {
      setErrorMessage("Username or password is wrong.");
    }
  }

  return (
    <Container>
      <Row>
        <Col sm={{ size: 4, offset: 4 }}>
          <h1 className="my-5 text-center">Login Page</h1>
          {errorMessage ? (
            <Alert color="danger">
              {errorMessage}
            </Alert>
          ) : ""}
          <Form onSubmit={handleSubmit}>
            <FormGroup className="mb-3">
              <Input type="text" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
            </FormGroup>

            <FormGroup className="mb-3">
              <Input type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
            </FormGroup>

            <div className="text-center">
              <Button color="primary" size="sm" type="submit">
                Login
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;