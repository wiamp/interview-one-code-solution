import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import {
  Container,
  Table,
  Button,
} from 'reactstrap';

import {
  getLocalStorage
} from '../components/LocalStorage';

const Detail = () => {
  if (!getLocalStorage("userData")) {
    window.location.href = "/login";
  }

  const [detailPost, setDetailPost] = useState({});

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    // fetch data
    const dataFetch = async () => {
      const resultPost = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/posts"
        )
      ).json();

      const resultUser = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/users"
        )
      ).json();

      for (let i in resultPost) {
        if (resultPost[i].id == params.postId) {
          const resultComment = await (
            await fetch(
              `https://jsonplaceholder.typicode.com/posts/${resultPost[i].id}/comments`
            )
          ).json();

          resultPost[i].comment = resultComment;

          for (let j in resultUser) {
            if (resultPost[i].userId === resultUser[j].id) {
              resultPost[i].userName = resultUser[j].name;
              break;
            }
          }

          // set state when the data received
          setDetailPost(resultPost[i]);

          break;
        }
      }
    };

    dataFetch();
  }, []);

  return (
    <Container>
      <div className="mb-3">
        <Button color="secondary" size="sm" onClick={() => navigate(-1)}>Back</Button>
      </div>
      <Table hover bordered striped responsive size="sm">
        <tbody>
          <tr>
            <td>{detailPost.userName}</td>
            <td>
              <div>{detailPost.title}</div>
              <div>{detailPost.body}</div>
            </td>
          </tr>
        </tbody>
      </Table>
      {detailPost?.comment ? (
        <div>
          All Comment
          <Table hover bordered striped responsive size="sm">
            <tbody>
              {detailPost.comment.map(comment => (
                <tr>
                  <td>{comment.name}</td>
                  <td>{comment.body}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      ) : null}
    </Container>
  );
};

export default Detail;