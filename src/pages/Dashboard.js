import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import {
  Container,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroup,
  Input,
  Button
} from 'reactstrap';

import {
  getLocalStorage
} from '../components/LocalStorage';

const Dashboard = () => {
  if (!getLocalStorage("userData")) {
    window.location.href = "/login";
  }

  const params = useParams();

  const [searchText, setSearchText] = useState("");
  // const [applySearchText, setApplySearchText] = useState("");
  const [resultPost, setResultPost] = useState([]);
  const [allPost, setAllPost] = useState([]);
  const [currentPage, setCurrentPage] = useState(params?.page || 1);
  const [totalPage, setTotalPage] = useState(1);

  useEffect(() => {
    // fetch data
    const dataFetch = async () => {
      setResultPost([]);
      setAllPost([]);

      const limit = 10; // limit of pagination

      const resultPost = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/posts"
        )
      ).json();

      // const resultAllPost = await (
      //   await fetch(
      //     "https://jsonplaceholder.typicode.com/posts"
      //   )
      // ).json();

      // console.log("applySearchText")
      // console.log(applySearchText)
      // for (let i in resultAllPost) {
      //   if (resultAllPost[i].title.search(applySearchText)) {
      //     setResultPost(resultPost => [...resultPost, resultAllPost[i]]);
      //   }
      // }

      const resultUser = await (
        await fetch(
          "https://jsonplaceholder.typicode.com/users"
        )
      ).json();

      let offset = currentPage * limit - limit;

      setTotalPage(Math.floor(resultPost.length / limit));

      for (let i = offset; i < offset + limit; i++) {
        const resultComment = await (
          await fetch(
            `https://jsonplaceholder.typicode.com/posts/${resultPost[i].id}/comments`
          )
        ).json();

        resultPost[i].totalComment = resultComment.length;

        for (let j in resultUser) {
          if (resultPost[i].userId === resultUser[j].id) {
            resultPost[i].userName = resultUser[j].name;
            break;
          }
        }

        // set state when the data received
        setAllPost(allPost => [...allPost, resultPost[i]]);
      }
    };

    dataFetch();
  }, [currentPage]);

  return (
    <Container>
      {/* <InputGroup>
        <Input type="text" placeholder="Search" onChange={(e) => setSearchText(e.target.value)} />
        <Button onClick={() => setApplySearchText(searchText)}>Search</Button>
      </InputGroup> */}
      <Table hover bordered striped responsive size="sm">
        <tbody>
          {allPost.map(post => (
            <tr>
              <td>{post.userName}</td>
              <td>
                <div>{post.title}</div>
                <div>
                  <a href={`/post/${post.id}/detail-comment`}>{post.totalComment}</a>&nbsp;
                  <a href={`/post/${post.id}/detail`}>Detail</a>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {totalPage > 1 ? (
        <div className="text-center">
          <Pagination aria-label="Page navigation example">
            {currentPage === 1 ? (
              <PaginationItem disabled>
                <PaginationLink previous href="#" />
              </PaginationItem>
            ) : (
              <PaginationItem >
                <PaginationLink previous href={`/dashboard/${currentPage - 1}`} />
              </PaginationItem>
            )}
            <PaginationItem disabled>
              <PaginationLink href="#">
                {currentPage}
              </PaginationLink>
            </PaginationItem>
            {currentPage === totalPage ? (
              <PaginationItem disabled>
                <PaginationLink next href="#" />
              </PaginationItem>
            ) : (
              <PaginationItem >
                <PaginationLink next onClick={() => {
                  setCurrentPage(currentPage + 1);
                  window.history.pushState(null, null, `/dashboard/${currentPage + 1}`);
                }} />
              </PaginationItem>
            )}
          </Pagination>
        </div>
      ) : ""}
    </Container>
  );
};

export default Dashboard;