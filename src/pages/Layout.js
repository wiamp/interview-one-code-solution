import { Component, useEffect } from 'react';
import { Outlet } from "react-router-dom";
import {
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Button,
} from 'reactstrap';
import { 
  setLocalStorage,
  getLocalStorage
} from '../components/LocalStorage'

export default class Layout extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const userData = getLocalStorage('userData');

    return (
      <Container>
        <Navbar color="light" light expand="md" className="mb-3">
          <NavbarBrand href="/dashboard">Cinta Coding</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} className="justify-content-end" navbar>
            <Nav className="ml-auto" navbar>
              {userData?.name ? (
                <NavItem>
                  Welcome, <a href="/profile">{userData.name}</a>
                </NavItem>
              ) : (
                <NavItem>
                  <Button color="primary" size="sm" href="/login">Login</Button>
                </NavItem>
              )}
            </Nav>
          </Collapse>
        </Navbar>
        <Outlet />
      </Container>
    );
  }
}