### REQUIREMENT

node v16.20.0

### INSTALLATION

- clone directory using `git clone` and use `development` as branch

- go to root directory

- use `npm install` to install packages

- use `npm start` to start development environment

### NOT DONE TASK

- some of styling

- search feature
